package com.andrei.wegroszta.codegen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArchGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArchGeneratorApplication.class, args);
	}
}
