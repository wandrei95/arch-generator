package com.andrei.wegroszta.codegen.service;

import com.squareup.javapoet.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.lang.model.element.Modifier;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class ArchGeneratorService {
    private final String GENERATED_FOLDER_NAME = "generated";

    public byte[] buildArchitectureFromFile(final MultipartFile file)
            throws RuntimeException {

        final String folderName = buildGeneratedFolderName();

        generateCode(file, folderName);

        return getGeneratedCodeZippedBytes(folderName);
    }

    private String buildGeneratedFolderName() {
        return System.currentTimeMillis() + "-" + ((int) (Math.random() * 100_000));
    }

    private void generateCode(final MultipartFile file, final String folderName) {
        final Document document = getDocument(file);

        if (document == null) {
            throw new RuntimeException();
        }

        generateX(document, ElementType.ENTITY, folderName);
        generateX(document, ElementType.USE_CASE, folderName);
        generateX(document, ElementType.PRESENTER, folderName);
        generateX(document, ElementType.VIEW, folderName);
    }

    private Document getDocument(final MultipartFile file) {
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            final DocumentBuilder builder = factory.newDocumentBuilder();
            return builder.parse(file.getInputStream());
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private byte[] getGeneratedCodeZippedBytes(final String folderName) {
        final String folder = GENERATED_FOLDER_NAME + File.separator + folderName;

        final ByteArrayOutputStream bOutput = new ByteArrayOutputStream();
        try {
            pack(folder, bOutput);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        clearCache(folder);

        return bOutput.toByteArray();
    }

    private void pack(final String sourceDirPath, final OutputStream outputStream) throws IOException {
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream)) {
            final Path sourceDir = Paths.get(sourceDirPath);
            Files.walk(sourceDir)
                    .filter(path -> !Files.isDirectory(path))
                    .forEach(path -> {
                        final ZipEntry zipEntry = new ZipEntry(sourceDir.relativize(path).toString());
                        try {
                            zipOutputStream.putNextEntry(zipEntry);
                            Files.copy(path, zipOutputStream);
                            zipOutputStream.closeEntry();
                        } catch (IOException e) {
                            e.printStackTrace();
                            throw new RuntimeException(e);
                        }
                    });
            zipOutputStream.finish();
        }
    }

    public static void clearCache(final String sourceDirPath) {
        final File f = new File(sourceDirPath);
        if (f.exists()) {
            if (f.isFile()) {
                f.delete();
                return;
            }
            File[] listFields = f.listFiles();
            if (listFields != null) {
                for (File child : f.listFiles()) {
                    clearCache(child.getPath());
                }
            }
            f.delete();
        }
    }

    private void generateX(final Document document, final ElementType elementType, final String folderName) {
        try {
            final String elemType = elementType.elementType;

            final NodeList elements = document.getDocumentElement().getElementsByTagName(elemType);
            for (int i = 0; i < elements.getLength(); i++) {

                final Element element = (Element) elements.item(i);

                final String elementName = element.getAttribute("name");
                final String elementPackage = element.getAttribute("package");

                final TypeSpec.Builder classBuilder = TypeSpec.classBuilder(elementName)
                        .addModifiers(Modifier.PUBLIC, Modifier.FINAL);
                final MethodSpec.Builder constructorBuilder = MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC);

                final NodeList listeners = element.getElementsByTagName("listener");

                for (int temp = 0; temp < listeners.getLength(); temp++) {
                    final Element listener = (Element) listeners.item(temp);
                    if (elemType.equals(listener.getParentNode().getParentNode().getNodeName())) {

                        final String listenerName = listener.getAttribute("name");

                        final TypeSpec.Builder listenerBuilder = TypeSpec.interfaceBuilder(listenerName).addModifiers(Modifier.PUBLIC);

                        final NodeList methods = listener.getElementsByTagName("method");
                        addMethods(listenerBuilder, methods);

                        classBuilder.addType(listenerBuilder.build());
                    }
                }

                final NodeList fieldsParent = element.getElementsByTagName("fields");
                if (fieldsParent != null) {
                    final Element e = ((Element) fieldsParent.item(0));
                    if (e != null) {
                        addFields(classBuilder, constructorBuilder, e, elementType);
                    }
                }

                final NodeList methodsParent = element.getElementsByTagName("methods");
                for (int temp = 0; temp < methodsParent.getLength(); temp++) {
                    final Element mp = (Element) methodsParent.item(temp);
                    if (elemType.equals(mp.getParentNode().getNodeName())) {
                        addMethods(classBuilder, mp.getElementsByTagName("method"));
                        break;
                    }
                }

                final NodeList repos = element.getElementsByTagName("repository");

                for (int temp = 0; temp < repos.getLength(); temp++) {
                    final Element repo = (Element) repos.item(temp);

                    final String listenerName = repo.getAttribute("name");

                    final TypeSpec.Builder repoBuilder = TypeSpec.interfaceBuilder(listenerName)
                            .addModifiers(Modifier.PUBLIC);

                    final NodeList repoMethodsParent = repo.getElementsByTagName("methods");
                    for (int t = 0; t < repoMethodsParent.getLength(); t++) {
                        final Element mp = (Element) repoMethodsParent.item(t);
                        if ("repository".equals(mp.getParentNode().getNodeName())) {
                            addMethods(repoBuilder, mp.getElementsByTagName("method"));
                            break;
                        }
                    }

                    final NodeList repoListeners = repo.getElementsByTagName("listener");

                    for (int t = 0; t < repoListeners.getLength(); t++) {
                        final Element listener = (Element) repoListeners.item(t);
                        if ("repository".equals(listener.getParentNode().getParentNode().getNodeName())) {

                            final String repoListenerName = listener.getAttribute("name");

                            final TypeSpec.Builder listenerBuilder = TypeSpec.interfaceBuilder(repoListenerName);

                            final NodeList methods = listener.getElementsByTagName("method");
                            addMethods(listenerBuilder, methods);

                            repoBuilder.addType(listenerBuilder.addModifiers(Modifier.PUBLIC, Modifier.STATIC).build());
                        }
                    }

                    final TypeSpec repoClass = repoBuilder.build();
                    final JavaFile repoFile = JavaFile.builder(elementPackage, repoClass)
                            .indent("    ")
                            .build();
                    repoFile.writeTo(new File(GENERATED_FOLDER_NAME + File.separator + folderName));
                }

                final NodeList interfacesParent = element.getElementsByTagName("interfaces");
                for (int temp = 0; temp < interfacesParent.getLength(); temp++) {
                    final Element mp = (Element) interfacesParent.item(temp);
                    if ("interfaces".equals(mp.getNodeName())) {
                        addSuperInterfaces(classBuilder, mp.getElementsByTagName("interface"));
                        break;
                    }
                }

                final MethodSpec constructor = constructorBuilder.build();
                final TypeSpec cls = classBuilder.addMethod(constructor).build();

                final JavaFile javaFile = JavaFile.builder(elementPackage, cls)
                        .indent("    ")
                        .build();
                javaFile.writeTo(new File(GENERATED_FOLDER_NAME + File.separator + folderName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addFields(final TypeSpec.Builder classBuilder,
                           final MethodSpec.Builder constructorBuilder,
                           final Element e,
                           final ElementType elementType) throws ClassNotFoundException {
        final NodeList fields = e.getElementsByTagName("field");

        for (int temp = 0; temp < fields.getLength(); temp++) {
            final Node node = fields.item(temp);
            final Element field = (Element) node;

            final String fieldName = field.getAttribute("name");
            final String fieldType = field.getAttribute("type");
            final String fieldTypePackage = field.getAttribute("typePackage");

            TypeName clazz = ClassName.get(fieldTypePackage, fieldType);

            final NodeList parameterizedTypes = field.getElementsByTagName("parameterizedType");
            if (parameterizedTypes != null && parameterizedTypes.getLength() > 0) {
                Element firstParameterizedType = (Element) parameterizedTypes.item(0);
                final String paramType = firstParameterizedType.getAttribute("type");
                final String paramTypePck = firstParameterizedType.getAttribute("typePackage");

                final ClassName parameterizedClassName = ClassName.get(fieldTypePackage, fieldType);
                final ClassName type = ClassName.get(paramTypePck, paramType);
                clazz = ParameterizedTypeName.get(parameterizedClassName, type);
            }

            final FieldSpec.Builder fieldBuilder = FieldSpec.builder(clazz, fieldName);

            final boolean b = Boolean.parseBoolean(field.getAttribute("constructor"));
            final String capitalizedFieldName = fieldName.substring(0, 1).toUpperCase()
                    + fieldName.substring(1);
            if (b) {
                constructorBuilder.addParameter(clazz, fieldName, Modifier.FINAL);
                constructorBuilder.addStatement("this.$N = $N", fieldName, fieldName);
                fieldBuilder.addModifiers(Modifier.PRIVATE, Modifier.FINAL);
            } else {
                fieldBuilder.addModifiers(Modifier.PRIVATE);
                final MethodSpec.Builder setter = MethodSpec.methodBuilder("set" + capitalizedFieldName)
                        .returns(void.class)
                        .addParameter(clazz, fieldName, Modifier.FINAL)
                        .addModifiers(Modifier.PUBLIC)
                        .addStatement("this.$N = $N", fieldName, fieldName);
                classBuilder.addMethod(setter.build());
            }

            if (elementType == ElementType.ENTITY) {
                final MethodSpec.Builder getter = MethodSpec.methodBuilder("get" + capitalizedFieldName)
                        .returns(clazz)
                        .addModifiers(Modifier.PUBLIC)
                        .addStatement("return this.$N", fieldName);
                classBuilder.addMethod(getter.build());
            }

            classBuilder.addField(fieldBuilder.build());
        }

        final NodeList bindingList = e.getElementsByTagName("bindingList");
        for (int temp = 0; temp < bindingList.getLength(); temp++) {
            final Node node = bindingList.item(temp);
            final Element field = (Element) node;

            final String fieldName = field.getAttribute("name");
            final String parametrizedType = field.getAttribute("parametrizedType");
            final String parametrizedTypePackage = field.getAttribute("parametrizedTypePackage");

            final ClassName list = ClassName.get("java.util", "List");
            final ClassName arrayList = ClassName.get("java.util", "ArrayList");
            final ClassName binding = ClassName.get(parametrizedTypePackage, parametrizedType);
            final TypeName listOfBindings = ParameterizedTypeName.get(list, binding);

            final FieldSpec.Builder fieldBuilder = FieldSpec.builder(listOfBindings, fieldName)
                    .addModifiers(Modifier.PRIVATE, Modifier.FINAL);

            constructorBuilder.addStatement("this.$N = new $T<>()", fieldName, arrayList);

            classBuilder.addField(fieldBuilder.build());

            final ClassName clazz = ClassName.get(parametrizedTypePackage, parametrizedType);

            final String paramName = parametrizedType.substring(0, 1).toLowerCase() + parametrizedType.substring(1);

            final MethodSpec.Builder bindMethodBuilder = MethodSpec.methodBuilder("bind" + parametrizedType)
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(clazz, paramName, Modifier.FINAL)
                    .addStatement("this.$N.add($L)", fieldName, paramName);
            classBuilder.addMethod(bindMethodBuilder.build());

            final MethodSpec.Builder unbindMethodBuilder = MethodSpec.methodBuilder("unBind" + parametrizedType)
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(clazz, paramName, Modifier.FINAL)
                    .addStatement("this.$N.remove($L)", fieldName, paramName);
            classBuilder.addMethod(unbindMethodBuilder.build());
        }
    }

    private void addSuperInterfaces(final TypeSpec.Builder classBuilder, final NodeList interfaces) {
        final int intLength = interfaces.getLength();

        for (int i = 0; i < intLength; i++) {
            final Element interf = (Element) interfaces.item(i);
            final String intName = interf.getAttribute("name");
            final String intPackage = interf.getAttribute("package");

            addMethods(classBuilder, interf.getElementsByTagName("method"));

            classBuilder.addSuperinterface(ClassName.get(intPackage, intName));
        }
    }

    private void addMethods(final TypeSpec.Builder classBuilder, final NodeList methods) {
        for (int j = 0; j < methods.getLength(); j++) {
            final Element method = (Element) methods.item(j);

            final String methodName = method.getAttribute("name");

            final MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder(methodName).addModifiers(Modifier.PUBLIC);

            boolean isAbstractMethod = Boolean.parseBoolean(method.getAttribute("abstract"));
            if (isAbstractMethod) {
                methodBuilder.addModifiers(Modifier.ABSTRACT);
            }

            final boolean isOverride = Boolean.parseBoolean(method.getAttribute("override"));
            if (isOverride) {
                methodBuilder.addAnnotation(Override.class);
            }

            final NodeList parameters = method.getElementsByTagName("parameter");
            for (int k = 0; k < parameters.getLength(); k++) {
                final Element parameter = (Element) parameters.item(k);

                if (!"method".equals(parameter.getParentNode().getNodeName())) {
                    continue;
                }

                final String paramName = parameter.getAttribute("name");
                final String paramType = parameter.getAttribute("type");
                final String paramTypePackage = parameter.getAttribute("typePackage");

                TypeName clazz = ClassName.get(paramTypePackage, paramType);

                final NodeList parameterizedTypes = parameter.getElementsByTagName("parameterizedType");
                if (parameterizedTypes != null && parameterizedTypes.getLength() > 0) {
                    Element firstParameterizedType = (Element) parameterizedTypes.item(0);
                    final String parameterizedType = firstParameterizedType.getAttribute("type");
                    final String paramTypePck = firstParameterizedType.getAttribute("typePackage");

                    final ClassName parameterizedClassName = ClassName.get(paramTypePackage, paramType);
                    final ClassName type = ClassName.get(paramTypePck, parameterizedType);
                    clazz = ParameterizedTypeName.get(parameterizedClassName, type);
                }

                methodBuilder.addParameter(clazz, paramName, Modifier.FINAL);
            }

            if (!isAbstractMethod) {
                addRepoCallbacksIfNeeded(method, methodBuilder);
            }

            final NodeList callCodes = method.getElementsByTagName("callCode");
            for (int k = 0; k < callCodes.getLength(); k++) {
                final Element callCode = (Element) callCodes.item(k);

                final String objectToCall = callCode.getAttribute("objectToCall");
                final String methodToCall = callCode.getAttribute("methodToCall");
                final NodeList callCodeParams = callCode.getElementsByTagName("callParameter");

                boolean shouldAddComma = false;
                final StringBuilder sb = new StringBuilder();
                for (int i = 0; i < callCodeParams.getLength(); i++) {
                    final Element callCodeParam = (Element) callCodeParams.item(i);

                    if (!"callCode".equals(callCodeParam.getParentNode().getNodeName())) {
                        continue;
                    }

                    final String paramName = callCodeParam.getAttribute("name");

                    if (shouldAddComma) {
                        sb.append(", ");
                    }
                    sb.append(paramName);
                    shouldAddComma = true;
                }
                methodBuilder.addStatement("$N.$N($L)", objectToCall, methodToCall, sb.toString());
            }

            classBuilder.addMethod(methodBuilder.build());
        }
    }

    private void addRepoCallbacksIfNeeded(final Element method, final MethodSpec.Builder methodBuilder) {
        final NodeList callbackInfo = method.getElementsByTagName("callBackInfo");

        if (callbackInfo == null || callbackInfo.item(0) == null) {
            return;
        }

        final Element callback = (Element) callbackInfo.item(0);
        final String callBackName = callback.getAttribute("callBackName");
        final String callBackMethodName = callback.getAttribute("callBackMethodName");
        final String callBackClass = callback.getAttribute("callBackClass");
        final String callBackClassPackage = callback.getAttribute("callBackClassPackage");

        final ClassName lc = ClassName.get(callBackClassPackage, callBackClass);

        final TypeSpec.Builder listenerBuilder = TypeSpec.anonymousClassBuilder("")
                .addSuperinterface(lc);

        final NodeList callbacks = callback.getElementsByTagName("callBack");

        for (int i = 0; i < callbacks.getLength(); i++) {
            final Element cb = (Element) callbacks.item(i);

            final MethodSpec callbackMethod = buildCallbackMethod(cb);

            listenerBuilder.addMethod(callbackMethod);
        }

        final NodeList callbackParams = ((Element) callbackInfo.item(0)).getElementsByTagName("parameter");
        final StringBuilder sb = new StringBuilder();
        boolean shouldAddComma = false;
        for (int k = 0; k < callbackParams.getLength(); k++) {
            final Element parameter = (Element) callbackParams.item(k);

            if (!"callBackInfo".equals(parameter.getParentNode().getNodeName())) {
                continue;
            }

            final String paramName = parameter.getAttribute("name");

            if (shouldAddComma) {
                sb.append(", ");
            }
            sb.append(paramName);
            shouldAddComma = true;
        }

        final String paramList = sb.toString();

        if (paramList.isEmpty()) {
            methodBuilder.addStatement("$N.$N($L)", callBackName, callBackMethodName, listenerBuilder.build());
        } else {
            methodBuilder.addStatement("$N.$N($L, $L)", callBackName, callBackMethodName, sb.toString(), listenerBuilder.build());
        }
    }

    private MethodSpec buildCallbackMethod(final Element callbackElem) {
        final String callbackName = callbackElem.getAttribute("name");
        final String listenerName = callbackElem.getAttribute("listenerName");
        final String listenerMethodName = callbackElem.getAttribute("listenerMethodName");

        final MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder(callbackName)
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL);

        final NodeList callbackParams = callbackElem.getElementsByTagName("parameter");

        final StringBuilder methodSB = new StringBuilder();
        boolean shouldAddComma = false;
        for (int k = 0; k < callbackParams.getLength(); k++) {
            final Element parameter = (Element) callbackParams.item(k);

            if (!"callBack".equals(parameter.getParentNode().getNodeName())) {
                continue;
            }

            final String paramName = parameter.getAttribute("name");
            final String paramType = parameter.getAttribute("type");
            final String paramTypePackage = parameter.getAttribute("typePackage");

            if (shouldAddComma) {
                methodSB.append(", ");
            }
            methodSB.append(paramName);
            shouldAddComma = true;

            TypeName clazz = ClassName.get(paramTypePackage, paramType);

            final NodeList parameterizedTypes = parameter.getElementsByTagName("parameterizedType");
            if (parameterizedTypes != null && parameterizedTypes.getLength() > 0) {
                Element firstParameterizedType = (Element) parameterizedTypes.item(0);
                final String parameterizedType = firstParameterizedType.getAttribute("type");
                final String paramTypePck = firstParameterizedType.getAttribute("typePackage");

                final ClassName parameterizedClassName = ClassName.get(paramTypePackage, paramType);
                final ClassName type = ClassName.get(paramTypePck, parameterizedType);
                clazz = ParameterizedTypeName.get(parameterizedClassName, type);
            }

            methodBuilder.addParameter(clazz, paramName, Modifier.FINAL);
        }

        final NodeList bindingList = callbackElem.getElementsByTagName("bindingList");
        final int bindingLength = bindingList.getLength();

        if (bindingLength > 0) {
            final Node node = bindingList.item(0);
            final Element field = (Element) node;

            final String fieldName = field.getAttribute("name");
            final String parametrizedType = field.getAttribute("parametrizedType");

            methodBuilder.returns(void.class)
                    .addComment("TODO Add your business logic here")
                    .beginControlFlow("for ($L $L : $N)", parametrizedType, "listener", fieldName)
                    .addStatement("$L.$L($L)", "listener", listenerMethodName, methodSB.toString())
                    .endControlFlow();
        } else {
            methodBuilder.returns(void.class)
                    .beginControlFlow("if ($N != null)", listenerName)
                    .addComment("TODO Add your business logic here")
                    .addStatement("$N.$N($L)", listenerName, listenerMethodName, methodSB.toString())
                    .endControlFlow();
        }

        return methodBuilder.build();
    }

    enum ElementType {
        ENTITY("entities", "entity"),
        USE_CASE("useCases", "useCase"),
        PRESENTER("presenters", "presenter"),
        VIEW("views", "view");

        private final String elemTypeParent;
        private final String elementType;

        ElementType(String elemTypeParent, String elementType) {
            this.elemTypeParent = elemTypeParent;
            this.elementType = elementType;
        }
    }
}