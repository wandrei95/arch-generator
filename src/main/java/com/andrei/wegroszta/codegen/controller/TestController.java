package com.andrei.wegroszta.codegen.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @CrossOrigin
    @GetMapping("/test")
    public String getTest() {
        return "{\"msg\": \"test works\"}";
    }
}
