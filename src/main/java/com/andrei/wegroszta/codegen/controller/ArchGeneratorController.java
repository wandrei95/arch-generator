package com.andrei.wegroszta.codegen.controller;

import com.andrei.wegroszta.codegen.service.ArchGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse ;

@RestController
public class ArchGeneratorController {

    private final ArchGeneratorService archGeneratorService;

    @Autowired
    public ArchGeneratorController(final ArchGeneratorService archGeneratorService) {
        this.archGeneratorService = archGeneratorService;
    }

    @CrossOrigin
    @PostMapping(value = "/build", produces = "application/zip")
    public ResponseEntity<byte[]> buildArch(@RequestParam("file") final MultipartFile file,
                                            final HttpServletResponse response) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/octet-stream");
        headers.add("Content-Disposition", "attachment; filename=\"zipFile.zip\"");

        byte[] bytes = archGeneratorService.buildArchitectureFromFile(file);

        return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
    }
}
